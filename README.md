# EARLY WORK - NOT YET USABLE

<div align="center">
<p align="center"><img src="https://gitlab.com/troll-engine/platform-browser/raw/master/logo.svg" align="center" width="350" height="250" alt="Project icon"></p>

<blockquote>
<p align="center">The Browser platform project for Troll Engine</p>
</blockquote>

<p align="center">
  <a href="https://gitlab.com/troll-engine/platform-browser/commits/master" alt="pipeline status"><img src="https://gitlab.com/troll-engine/platform-browser/badges/master/pipeline.svg" /></a>
  <a href="https://gitlab.com/troll-engine/platform-browser/commits/master" alt="coverage report"><img src="https://gitlab.com/troll-engine/platform-browser/badges/master/coverage.svg" /></a>
  <a href="https://www.npmjs.com/package/@troll-engine/platform-browser" alt="version"><img src="https://badgen.net/npm/v/@troll-engine/platform-browser" /></a>
  <a href="https://www.npmjs.com/package/@troll-engine/platform-browser" alt="license"><img src="https://badgen.net/npm/license/@troll-engine/platform-browser" /></a>
  <a href="https://www.npmjs.com/package/@troll-engine/platform-browser" alt="downloads"><img src="https://badgen.net/npm/dt/@troll-engine/platform-browser" /></a>
  <a href="https://discord.gg/ewwaSxv" alt="discord"><img src="https://discordapp.com/api/guilds/621750483098271745/widget.png" /></a>
</p>

</div>
</p>

```bash
npm install --save @troll-engine/platform-browser
```
